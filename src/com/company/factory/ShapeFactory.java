package com.company.factory;

import com.company.domain.*;

import java.util.Random;

public class ShapeFactory {

    public static Shape makeShape(String shapeType) {

        switch (shapeType.trim()) {

            case "Line":
                return getRandomLine();

            case "Rectangle":
                return new Rectangle();

            case "Triangle":
                return new Triangle();

            case "ComplexShape":
                return new ComplexShape();

            default:
                return null;
        }
    }

    private static Line getRandomLine() {
        Random rand = new Random();
        return new Line(rand.nextInt((100 - 1) + 1) + 1, rand.nextInt((100 - 1) + 1) + 1, rand.nextInt((100 - 1) + 1) + 1, rand.nextInt((100 - 1) + 1) + 1);

    }
}
