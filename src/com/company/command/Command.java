package com.company.command;

import com.company.decorators.ShapeColorDecorator;
import com.company.domain.Line;
import com.company.domain.Shape;

public class Command {
    private Shape shape;

    public Command(Shape shape) {
        this.shape = shape;
    }

    public void printFullShape() {
        Line[] lineList;
        if (shape instanceof Line) {
            lineList = new Line[]{(Line) shape};
        } else {
            lineList = (Line[]) shape.explodeShape();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Shape: ");
        for (Line line : lineList) {
            sb.append(line.point1X);
            sb.append(',');
            sb.append(line.point1Y);
            sb.append(" ");
            sb.append(line.point2X);
            sb.append(',');
            sb.append(line.point2Y);
            sb.append(" ");
        }

        if (shape instanceof ShapeColorDecorator){
            sb.append("color: ");
            sb.append(((ShapeColorDecorator) shape).getColor());
        }
        System.out.println(sb.toString());
    }

    public void printCordinates() {
        Line[] lineList;
        if (shape instanceof Line) {
            lineList = new Line[]{(Line) shape};
        } else {
            lineList = (Line[]) shape.explodeShape();
        }

        StringBuilder sb = new StringBuilder();
        for (Line line : lineList) {
            sb.append(line.point1X);
            sb.append(',');
            sb.append(line.point1Y);
            sb.append(" ");
            sb.append(line.point2X);
            sb.append(',');
            sb.append(line.point2Y);
            sb.append(" ");
        }
        System.out.println(sb.toString());
    }


}
