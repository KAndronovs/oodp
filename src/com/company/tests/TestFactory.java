package com.company.tests;

import com.company.decorators.*;
import com.company.domain.*;
import com.company.factory.*;
import org.junit.*;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class TestFactory {

    @Test
    public void testFactory() {
        ShapeFactory shapeFactory = new ShapeFactory();
//
//        //get an object of Rectangle and call its draw method.
//        Shape shape1 = shapeFactory.makeShape("Line");
//
//        //call draw method of Rectangle
//        shape1.explodeShape();

        //get an object of Rectangle and call its draw method.
        Shape shape2 = shapeFactory.makeShape("Rectangle");

        //call draw method of Rectangle
        shape2.explodeShape();

        //get an object of Square and call its draw method.
        Shape shape3 = shapeFactory.makeShape("Triangle");

        //call draw method of circle
        shape3.explodeShape();

        //decorators
        ShapeBlueColorDecorator shape4 = new ShapeBlueColorDecorator(shapeFactory.makeShape("Rectangle"));
        assertEquals(shape4.getColor(), "Beautiful blue");
        assertThat(shape4, instanceOf(Shape.class));

        ShapeRedColorDecorator shape5 = new ShapeRedColorDecorator(shapeFactory.makeShape("Circle"));
        assertEquals(shape5.getColor(), "Bloody red");
        assertThat(shape5, instanceOf(Shape.class));
    }
}
