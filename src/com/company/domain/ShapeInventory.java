package com.company.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShapeInventory implements Iterator<Shape> {
    private List<Shape> shapeList = new ArrayList();
    private Iterator<Shape> shapeIterator = shapeList.listIterator();

    public Shape next() {
        return shapeIterator.next();
    }

    @Override
    public boolean hasNext() {
        return shapeIterator.hasNext();
    }

    public void add(Shape shape) {
        shapeList.add(shape);
        shapeIterator = shapeList.listIterator();
    }
}
