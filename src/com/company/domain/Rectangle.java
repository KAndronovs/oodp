package com.company.domain;

import com.sun.org.apache.regexp.internal.RE;
import sun.nio.cs.ext.ISCII91;

public class Rectangle implements Shape {

        // List of shapes forming the rectangle
        // rectangle is centered around origin
        Line[] rectangleEdges = {new Line(-1,-1,1,-1),new Line(-1,1,1,1),new Line(-1,-1,-1,1),new Line(1,-1,1,1)};
    
        @Override
        public Shape[] explodeShape() {

            return rectangleEdges;

        }

    /**
     *
     * Although this method applies to composites only
     * it has been added to interface to enhance uniformity
     *
     * @param shape
     */
    public void addToShape(Shape shape) {
        
    }

}
