package com.company.domain;

public class Triangle implements Shape {

    Line[] traingleEdges = {new Line(-1,-1,1,-1),new Line(-1,1,1,1),new Line(-1,-1,-1,1),new Line(1,-1,1,1)};

    @Override
    public Shape[] explodeShape() {

        return traingleEdges;

    }

    /**
     *
     * Although this method applies to composites only
     * it has been added to interface to enhance uniformity
     *
     * @param shape
     */
    public void addToShape(Shape shape) {

    }
}
