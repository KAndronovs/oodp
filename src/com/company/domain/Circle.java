//package com.company.domain;
//
//public class Circle implements Shape {
//    private final double radius;
//
//    public Circle() {
//        this(1);
//    }
//    public Circle(double radius) {
//        this.radius = radius;
//    }
//
//    @Override
//    public double area() {
//        // A = π r^2
//        return Math.PI * Math.pow(radius, 2);
//    }
//
//    public double perimeter() {
//        // P = 2πr
//        return 2 * Math.PI * radius;
//    }
//
//    @Override
//    public void draw() {
//        System.out.println("Inside Circle::draw() method.");
//    }
//
//}
