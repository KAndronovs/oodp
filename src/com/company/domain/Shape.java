package com.company.domain;

public interface  Shape {

//    double area();

//    double perimeter();


    /**
     *
     * Although this method applies to composites only
     * it has been added to interface to enhance uniformity
     *
     * @param shape
     */
    void addToShape(Shape shape);


    /**
     * Making a complex shape explode results in getting a list of the
     * shapes forming this shape
     *
     *  For example if a rectangle explodes it results in 4 line objects
     *
     * Making a simple shape explode results in returning the shape itself
     */
    public Shape[] explodeShape();

}
