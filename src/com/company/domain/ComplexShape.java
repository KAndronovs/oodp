package com.company.domain;

import java.util.*;

public class ComplexShape implements Shape {


    /**
     * List of shapes
     */
    List lineList = new ArrayList<Line>();

    /**
     *
     */
    public void addToShape(Shape shapeToAddToCurrentShape) {
        Line[] lines = (Line[]) shapeToAddToCurrentShape.explodeShape();
        lineList.addAll(Arrays.asList(lines));

    }


    public Shape[] explodeShape() {
        return (Shape[]) lineList.toArray(new Line[lineList.size()]);

    }


}
