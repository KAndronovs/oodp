package com.company.decorators;

import com.company.domain.Shape;

public class ShapeRedColorDecorator extends ShapeColorDecorator {

    public ShapeRedColorDecorator(Shape decoratedShape){
        super(decoratedShape);
    }

    public String getColor() {
        return "Bloody red";
    }
}
