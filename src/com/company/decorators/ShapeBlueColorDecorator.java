package com.company.decorators;

import com.company.domain.*;

public class ShapeBlueColorDecorator extends ShapeColorDecorator {

    public ShapeBlueColorDecorator(Shape decoratedShape){
        super(decoratedShape);
    }

    public String getColor() {
        return "Beautiful blue";
    }
}
