package com.company.decorators;

import com.company.domain.*;

public abstract class ShapeColorDecorator implements Shape {

    protected Shape decoratedShape;

    public ShapeColorDecorator(Shape decoratedShape){
        this.decoratedShape = decoratedShape;
    }

    @Override
    public void addToShape(Shape shape) {
        shape.addToShape(shape);
    }

    @Override
    public Shape[] explodeShape() {
        return decoratedShape.explodeShape();
    }

    public abstract String getColor();

}
