package com.company;

import com.company.command.Command;
import com.company.decorators.ShapeBlueColorDecorator;
import com.company.decorators.ShapeRedColorDecorator;
import com.company.domain.*;
import com.company.factory.ShapeFactory;

public class Main {

    public static void main(String[] args) {
        System.out.println("Creating new shape inventory");
        ShapeInventory shapeInventory = new ShapeInventory();
        System.out.println("Creating Line shape line1");
        Line line1 = (Line) ShapeFactory.makeShape("Line");
        System.out.println("Creating Line shape line2");
        Line line2 = (Line) ShapeFactory.makeShape("Line");
        System.out.println("Adding line1 to shape inventory");
        shapeInventory.add(line1);
        System.out.println("Adding line2 to shape inventory");
        shapeInventory.add(line2);
        System.out.println("Creating new Rectangle shape and adding to shape inventory");
        shapeInventory.add(ShapeFactory.makeShape("Rectangle"));
        System.out.println("Creating new Rectangle shape with Blue shape decorator and adding to shape inventory");
        shapeInventory.add(new ShapeBlueColorDecorator(ShapeFactory.makeShape("Rectangle")));
        System.out.println("Creating new Rectangle shape with Red shape decorator and adding to shape inventory");
        shapeInventory.add(new ShapeRedColorDecorator(ShapeFactory.makeShape("Rectangle")));
        System.out.println("Creating new Triangle shape and adding to shape inventory");
        shapeInventory.add(ShapeFactory.makeShape("Triangle"));
        System.out.println("Creating new Complex shape");
        Shape complexShape = ShapeFactory.makeShape("ComplexShape");
        System.out.println("Creating new Rectangle to the Complex shape");
        complexShape.addToShape(ShapeFactory.makeShape("Rectangle"));
        System.out.println("Creating new Triangle to the Complex shape");
        complexShape.addToShape(ShapeFactory.makeShape("Triangle"));
        System.out.println("Adding triangle to the shape inventory");
        shapeInventory.add(complexShape);

        System.out.println("Creating new command");
        Command command;
        System.out.println("Itterating trough shape inventory: ");
        int i = 1;
        while (shapeInventory.hasNext()) {
            command = new Command(shapeInventory.next());
            System.out.println("");
            System.out.println("SHAPE No." + ++i);
            System.out.println("Printing only cordinates:");
            command.printCordinates();
            System.out.println("");
            System.out.println("Printing cordinates and some info:");
            command.printFullShape();
        }


    }
}
